Prvi domaći zadatak za grupu 2

Kreirati aplikaciju za prodaju satova. U tu svrhu kreirati klasu Sat koja sadrži:
• Zaštićene atribute:
• proizvodjač
• model,
• pol (može da uzme vrednost: ZaMuškarce, ZaŽene, Univerzalan,
kreirati enumeraciju sa ovim vrednostima),
• godinu proizvodnje modela,
• cenu;

• Javne metode:
• konstruktor koji postavlja vrednosti svih atributa,
• virtuelnu metodu za računanje popularnosti sata (računa se po formuli
1/((trenutnaGodina – godina)*cena) ),
• virtuelnu metodu koja prikazuje sve atribute klase na standardni izlaz;
• Javno svojstvo (property) koje vraća popularnost sata.

Kreirati i sledeće klase izvedene iz klase Sat:

• Ručni sat - koja sadrži dodatne privatne atribute: tip (može da uzme
vrednosti iz opsega: Automatik, Kvarcni, Solarni, kreirati enumaraciju
sa ovim vrednostima), težinu i da li je sa pozlatom
• Zidni sat - koja sadrži dodatni privatni atribut dimenziju sata koja je
opisana dužinom, širinom i visinom.
U izvedenim klasama definisati konstruktore koji postavljaju sve atribute klasa i
predefinisati metode za izračunavanje popularnosti sata i za prikaz na standardni izlaz.
Metoda za prikaz treba da prikaže najpre sve vrednosti atributa is klase Sat, a zatim sve
atribute iz izvedene klase.

Popularnost satova se računa na sledeći način:

• Za ručne satove: PopularnostIzOsnovneKlaseSat *1.5 + (35% ukoliko je
sat sa pozlatom)
• Za zidne satove: PopularnostIzOsnovneKlaseSat + (dužina + širina +
visina)/100

U funkciji main kreirati niz Satova (pretpostaviti da ih ne može biti više od 30), učitati
podatke o nekoliko satova (pre učitavanja podataka o satu učitati o kojoj vrsti sata se
radi), zatim prikazati listu svih satova, popularnost svih satova i prikazati najpopularniji i
najskuplji sat koji je trenutno na prodaji.