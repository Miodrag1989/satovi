﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_2
{
    class Sat
    {
        protected const ushort trenutnaGodina = 2019;
        protected string proizvodjac;
        protected string model;
        protected Enumeracije.Pol pol;
        protected ushort godina;
        protected float cena;


        public Sat(string proizvodjac, string model, Enumeracije.Pol pol,
            ushort godina, float cena)
        {
            this.proizvodjac = proizvodjac;
            this.model = model;
            this.pol = pol;
            this.godina = godina;
            this.cena = cena;
        }
        public Sat(Sat s)
        {
            this.proizvodjac = s.proizvodjac;
            this.model = s.model;
            this.pol = s.pol;
            this.godina = s.godina;
            this.cena = s.cena;
        }

        public float Cena { get { return this.cena; } }

        public virtual float Popularnost()
        {
            return 1 / ((trenutnaGodina - this.godina) * this.cena);
        }

        public virtual void Prikaz()
        {
            Console.WriteLine($"Proizvodjac: {this.proizvodjac} Model: {this.model}" +
                $" Za: {this.pol} Godina Proizvodnje: {this.godina} Cena: {this.cena}" +
                $" Popularnost: {this.Popularnost()}");
        }
    }
}
