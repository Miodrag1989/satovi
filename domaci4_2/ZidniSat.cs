﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_2
{
    class ZidniSat : Sat
    {
        private float[] dimenzije = new float[3];

        public ZidniSat(string proizvodjac, string model,
            Enumeracije.Pol pol, ushort godina, float cena, float d, float s,float h)
            :base(proizvodjac,model,pol,godina,cena)
        {
            this.dimenzije[0] = d;
            this.dimenzije[1] = s;
            this.dimenzije[2] = h;
        }

        public ZidniSat(ZidniSat zs)
            :base(zs.proizvodjac, zs.model, zs.pol, zs.godina, zs.cena)
        {
            this.dimenzije = zs.dimenzije;
        }

        public override float Popularnost()
        {
            return base.Popularnost() + ((this.dimenzije[0] + this.dimenzije[1] + 
                this.dimenzije[2])/100);
        }

        public override void Prikaz()
        {
            Console.WriteLine("Zidni Sat:");
            base.Prikaz();
            Console.WriteLine($"Dimenzija: ({this.dimenzije[0]},{this.dimenzije[1]}," +
                $"{this.dimenzije[2]})");
        }
    }
}
