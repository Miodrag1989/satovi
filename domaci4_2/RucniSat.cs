﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_2
{
    class RucniSat : Sat
    {
        private Enumeracije.Tip tip;
        private float tezina;
        private Enumeracije.Pozlata pozlata;

        public RucniSat(string proizvodjac,string model,Enumeracije.Pol pol, 
            ushort godina,float cena,float tezina, Enumeracije.Pozlata pozlata,
            Enumeracije.Tip tip)
            :base(proizvodjac,model,pol,godina,cena)
        {
            this.tip = tip;
            this.tezina = tezina;
            this.pozlata = pozlata;
        }
        public RucniSat(RucniSat rs)
            : base(rs.proizvodjac, rs.model, rs.pol, rs.godina, rs.cena)
        {
            this.tip = rs.tip;
            this.tezina = rs.tezina;
            this.pozlata = rs.pozlata;
        }

        public override float Popularnost()
        {
            float osnova = base.Popularnost()*1.5f;
            if (this.pozlata == Enumeracije.Pozlata.Da)
            {
                return osnova * 1.35f;
            }
            return osnova;
        }

        public override void Prikaz()
        {
            Console.WriteLine($"Rucni sat:");
            base.Prikaz();
            Console.WriteLine($"Tip: {this.tip} Tezine: {this.tezina} Pozlata: {this.pozlata}");
        }


    }
}
