﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci4_2
{
    class Program
    {
        public static void Ucitaj(out string proizvodjac,out string model, 
            out Enumeracije.Pol pol, out ushort godina, 
            out float cena)
        {
            pol = Enumeracije.Pol.Univerzalno;
            Console.WriteLine("Proizvodjac:");
            proizvodjac = Console.ReadLine();
            Console.WriteLine("Model:");
            model = Console.ReadLine();
            Console.WriteLine("Opcije: 1 - Za Muskarce; 2 - Za Zene; 3 - Univerzalno");
            int.TryParse(Console.ReadLine(), out int odabir);
            switch (odabir)
            {
                case 1:
                    pol = Enumeracije.Pol.ZaMuskarce;
                    break;
                case 2:
                    pol = Enumeracije.Pol.ZaZene;
                    break;
                case 3:
                    pol = Enumeracije.Pol.Univerzalno;
                    break;
            }
            Console.WriteLine("Godina proizvodnje:");
            ushort.TryParse(Console.ReadLine(), out godina);
            Console.WriteLine("Cena:");
            float.TryParse(Console.ReadLine(), out cena);
        }
        public static void Popularnost(ref Sat[] nizSatova, int ukupanBrojUnetihSatova)
        {
            int imin = 0;
            for (int i = 0; i < ukupanBrojUnetihSatova - 1; i++)
            {
                imin = i;
                for (int j = i + 1; j < ukupanBrojUnetihSatova; j++)
                {
                    if (nizSatova[imin].Popularnost() < nizSatova[j].Popularnost())
                        imin = j;
                }
                if (i != imin)
                {
                    Sat pom = nizSatova[imin];
                    nizSatova[imin] = nizSatova[i];
                    nizSatova[i] = pom;
                }
            }
        }
        public static void Najskuplji(ref Sat[] nizSatova, int ukupanBrojUnetihSatova)
        {
            int imin = 0;
            for (int i = 0; i < ukupanBrojUnetihSatova - 1; i++)
            {
                imin = i;
                for (int j = i + 1; j < ukupanBrojUnetihSatova; j++)
                {
                    if (nizSatova[imin].Cena < nizSatova[j].Cena)
                        imin = j;
                }
                if (i != imin)
                {
                    Sat pom = nizSatova[imin];
                    nizSatova[imin] = nizSatova[i];
                    nizSatova[i] = pom;
                }
            }
        }
        static void Main(string[] args)
        {
            int broj = 0;
            do
            {
                System.Console.WriteLine("Ukupan broj satova za unos(1 - 30):");
                int.TryParse(Console.ReadLine(), out broj);
            } while (broj <= 0 || broj > 30);
            Sat[] nizSatova = new Sat[broj];
            Console.WriteLine("Opcije: 1 - Unos sata; 2 - Prikaz satova; 0 - Izlaz");
            int.TryParse(Console.ReadLine(), out int unos);
            int ukupanBrojUnetihSatova = 0, pom = 0;
            while (unos != 0)
            {
                while (unos == 1 && ukupanBrojUnetihSatova != broj)
                {
                    Console.WriteLine("Broj satova za unos: ");
                    int.TryParse(Console.ReadLine(), out int brojSatovaZaUnos);
                    for (int i = ukupanBrojUnetihSatova; i < brojSatovaZaUnos + pom && i < nizSatova.Length; i++)
                    {
                        Console.WriteLine("Vrsta sata(1 - Rucni Sat; 2 - Zidni Sat):");
                        int.TryParse(Console.ReadLine(), out int vrsta);
                        while (vrsta < 1 || vrsta > 3)
                        {
                            Console.WriteLine("Vrsta vozila(1 - Rucni Sat; 2 - Zidni Sat):");
                            int.TryParse(Console.ReadLine(), out vrsta);
                        }
                        switch (vrsta)
                        {
                            case 1:
                                Console.WriteLine("Ucitavanje podataka o rucnom satu!");
                                Ucitaj(out string proizvodjac, out string model,out Enumeracije.Pol pol, out ushort godina,out float cena);
                                Enumeracije.Tip tip = Enumeracije.Tip.Automatik;
                                Console.WriteLine("Opcije (1 - Automatik; 2 - Kvarcni; 3 - Solarni):");
                                int.TryParse(Console.ReadLine(), out int odabir);
                                switch (odabir)
                                {
                                    case 1:
                                        tip = Enumeracije.Tip.Automatik;
                                        break;
                                    case 2:
                                        tip = Enumeracije.Tip.Kvarcni;
                                        break;
                                    case 3:
                                        tip = Enumeracije.Tip.Solarni;
                                        break;
                                }
                                Console.WriteLine("Tezina:");
                                float.TryParse(Console.ReadLine(), out float tezina);
                                Enumeracije.Pozlata pozlata = Enumeracije.Pozlata.Da;
                                Console.WriteLine("Pozlata (Opcije 0 - Ne; 1 - Da):");
                                int.TryParse(Console.ReadLine(), out int pozl);
                                switch (pozl)
                                {
                                    case 1:
                                        pozlata = Enumeracije.Pozlata.Ne;
                                        break;
                                    case 2:
                                        pozlata = Enumeracije.Pozlata.Da;
                                        break;
                                }
                                nizSatova[i] = new RucniSat(proizvodjac, model, pol, godina, cena, tezina,pozlata,tip);
                                break;
                            case 2:
                                Console.WriteLine("Ucitavanje podataka o zidnom satu!");
                                Ucitaj(out proizvodjac, out model, out pol, out godina, out cena);
                                Console.WriteLine("Duzina sata:");
                                float.TryParse(Console.ReadLine(), out float d);
                                Console.WriteLine("Sirina sata:");
                                float.TryParse(Console.ReadLine(), out float s);
                                Console.WriteLine("Visina sata:");
                                float.TryParse(Console.ReadLine(), out float h);
                                nizSatova[i] = new ZidniSat(proizvodjac, model, pol, godina, cena, d, s, h);
                                break;
                        }
                    }
                    pom = ukupanBrojUnetihSatova += brojSatovaZaUnos;

                    Console.WriteLine("Opcije: 1 - Unos satova; 2 - Prikaz satova; 0 - Izlaz");
                    int.TryParse(Console.ReadLine(), out unos);
                }
                while (ukupanBrojUnetihSatova == broj && unos == 1)
                {
                    Console.WriteLine("Lista je puna!");
                    Console.WriteLine("Opcije: 1 - Unos satova; 2 - Prikaz satova; 0 - Izlaz");
                    int.TryParse(Console.ReadLine(), out unos);
                }
                while (unos == 2)
                {
                    Console.WriteLine("Lista:");
                    for (int i = 0; i < ukupanBrojUnetihSatova; i++)
                    {
                        nizSatova[i].Prikaz();
                    }
                    Console.WriteLine();
                    Najskuplji(ref nizSatova, ukupanBrojUnetihSatova);
                    Console.WriteLine($"Najskupli sat je:");
                    nizSatova[0].Prikaz();
                    Popularnost(ref nizSatova, ukupanBrojUnetihSatova);
                    Console.WriteLine("Najpopularniji sat je:");
                    nizSatova[0].Prikaz();
                    Console.WriteLine();
                    Console.WriteLine("Opcije: 1 - Unos satova; 2 - Prikaz satova; 0 - Izlaz");
                    int.TryParse(Console.ReadLine(), out unos);

                }
            }
        }
    }
}
